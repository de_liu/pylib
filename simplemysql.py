#!/usr/bin/env python
# vim: fileencoding=utf-8: noexpandtab

"""
    A very simple wrapper for MySQLdb

    Methods:
        getOne() - get a single row
        getAll() - get all rows
        insert() - insert a row
        update() - update rows
        delete() - delete rows
        query()  - run a raw sql query
        leftJoin() - do an inner left join query and get results

    License: GNU GPLv2
    
    added by De: 
        inserts() - insert multiple records one by one
        insertOrUpdate() - insert a row or update it if it exists
        getIdOrInsert() - get the row's id if existing, or insert the row and return ID. 
        add logging function. 

    Kailash Nadh, http://nadh.in
    May 2013
"""

import pymysql
from collections import namedtuple, Iterable
import logging

class SimpleMysql:
    conn = None
    cur = None
    conf = None

    def __init__(self, **kwargs):
        self.conf = kwargs
        self.conf["keep_alive"] = kwargs.get("keep_alive", False)
        self.conf["charset"] = kwargs.get("charset", "utf8")
        self.conf["host"] = kwargs.get("host", "localhost")
        self.readonly = kwargs.get("readonly", False)
        self.logger = kwargs.get("logger", None)
        self.connected = False

        if self.logger is None:
            # add a default logger
            logging.basicConfig(level=logging.INFO)
            self.logger = logging.getLogger(__name__)

        # if not self.conf["readonly"]:
        # actually need a db connection
        self.connect()

    def connect(self):
        """Connect to the mysql server"""
        try:
            self.conn = pymysql.connect(db=self.conf['db'], host=self.conf['host'],
                                        user=self.conf['user'], passwd=self.conf['passwd'],
                                        charset=self.conf['charset'])
            self.cur = self.conn.cursor() 
            self.connected = True
        except:
            self.connected = False
            self.logger.error("MySQL connection failed")
            raise

    
    def getOne(self, table=None, fields='*', where=None, order=None, limit=[1]):
        """Get a single result

            table = (str) table_name
            fields = (field1, field2 ...) list of fields to select
            where = ("parameterizedstatement", tuple(parameters)
                    eg: ("id=%s and name=%s", (1, "test"))
            order = [field, ASC|DESC]
            limit = [limit1, limit2]
        """

        if not self.connected:
            return None

        cur = self._select(table, fields, where, order, limit)
        result = cur.fetchone()

        row = None
        if result:
            Row = namedtuple("Row", [f[0] for f in cur.description])
            row = Row(*result)

        return row


    def getAll(self, table=None, fields='*', where=None, order=None, limit=None):
        """Get all results

            table = (str) table_name
            fields = (field1, field2 ...) list of fields to select
            where = ("parameterizedstatement", [parameters])
                    eg: ("id=%s and name=%s", [1, "test"])
            order = [field, ASC|DESC]
            limit = [limit1, limit2]
        """
        if not self.connected:
            return None

        cur = self._select(table, fields, where, order, limit)
        result = cur.fetchall()
        
        rows = None
        if result:
            Row = namedtuple("Row", [f[0] for f in cur.description])
            rows = [Row(*r) for r in result]

        return rows


    def leftJoin(self, tables=(), fields=(), join_fields=(), where=None, order=None, limit=None):
        """Run an inner left join query

            tables = (table1, table2)
            fields = ([fields from table1], [fields from table 2])  # fields to select
            join_fields = (field1, field2)  # fields to join. field1 belongs to table1 and field2 belongs to table 2
            where = ("parameterizedstatement", [parameters])
                    eg: ("id=%s and name=%s", [1, "test"])
            order = [field, ASC|DESC]
            limit = [limit1, limit2]
        """
        if not self.connected:
            return None

        cur = self._select_join(tables, fields, join_fields, where, order, limit)
        result = cur.fetchall()
        
        rows = None
        if result:
            Row = namedtuple("Row", [f[0] for f in cur.description])
            rows = [Row(*r) for r in result]

        return rows


    def insert(self, table, data, ignore=True):
        """Insert a record. data is in the form of a dictionary"""

        query = self._serialize_insert(data)

        sql = "INSERT %s INTO %s (%s) VALUES(%s)" % ("IGNORE" if ignore else '', table, query[0], query[1])
        if self.readonly:
            self.logger.debug("insert")
        return self.query(sql, tuple(data.values())).rowcount
    
    def inserts(self, table, data, ignore=True):
        """Insert multiple records records at once. Data:dictionary"""
        rows = 0
        for row in data:
            rows += self.insert(table, row, ignore)
        return rows            

#    def update(self, table, data, where = None):
#        """update a record, :where: takes the form of ['field=%s and field>%s',[3,'xyz']] 
#           that is the first part is template, the second part is values.
#        """
#
#        query = self._serialize_update(data)
#
#        sql = "UPDATE %s SET %s" % (table, query)
#
#        if where and len(where) > 0:
#            sql += " WHERE %s" % where[0]
#
#        if self.readonly:
#            self.logger.debug("update")
#
#        return self.query(sql, tuple(data.values()) + where[1] if where and len(where) > 1 else tuple(data.values())
#                        ).rowcount
    def update(self, table, data, keys):
        """     
        Args:
            table: table name
            data: dictionary holding data
            keys: a list of field names used for construction where clause in case of updating
        Returns:
            updated: integer
        """
      
        # key part of the data
        data_key = {k:data[k] for k in keys}
        
        # non key part of the data
        for k in keys:
            del data[k]

            
        set_query = self._serialize_update(data)
        where_query = self._serialize_where(data_key)

        sql = "UPDATE %s SET %s WHERE %s" % (table, set_query, where_query)


        if self.readonly:
            self.logger.debug("update")

        return self.query(sql, tuple(data.values()) + tuple(data_key.values())).rowcount

    def insertOrUpdate(self, table, data, keys):
        """Insert a record, on duplicate key update; 

        Args:
            table: table name
            data: dictionary holding data
            keys: a list of field names used for construction where clause in case of updating
        Returns:
            [inserted,updated]: both integers.
        """
        insert_data = data.copy()
        update_data = data.copy()

        insert = self._serialize_insert(insert_data)

        for k in keys:
            del update_data[k]

        update = self._serialize_update(update_data)

        sql = "INSERT INTO %s (%s) VALUES(%s) ON DUPLICATE KEY UPDATE %s" % (table, insert[0], insert[1], update)
        
#        self.logger.debug("sql")
#        self.logger.debug(sql)
#        self.logger.debug("params")
#        self.logger.debug(tuple(insert_data.values()) + tuple(update_data.values()))

        if self.readonly:
            self.logger.debug("insertOrUpdate")

        rowCount = self.query(sql, tuple(insert_data.values()) + tuple(update_data.values())).rowcount
        if rowCount==1:
            return [1,0]
        elif rowCount == 2:
            return [0,1]
        else:
            return [0,0]

    def getIdOrInsert(self, table, lookup_data, id_field,additional_data={}):
        """get the id of the record or insert it; 
        Args:
            table: table name
            lookup_data: dictionary holding critera for looking up
            id_field: field name for id column (to be returned as id)
            additional_data: only used in the case of insertion insert 
        Returns:
            [id,inserted]: id of the record, # of records inserted.

        """
        if self.readonly:
            self.logger.debug("getIdOrInsert")

        
        row = self.getOne(table,[id_field],[self._serialize_where(lookup_data),tuple(lookup_data.values())])
        if row is None: 
            # did not exist, do insert.
            all_data = additional_data.copy() # merge dictionary
            all_data.update(lookup_data)
            inserted = self.insert(table, all_data)
            return([self.cur.lastrowid,inserted]) #use lasrowid instead of insert_id() the latter does not seem to work.
        else:
            return([row[0],0])

    def delete(self, table, where = None):
        """Delete rows based on a where condition; returns affected rows"""

        sql = "DELETE FROM %s" % table

        if where and len(where) > 0:
            sql += " WHERE %s" % where[0]
        if self.readonly:
            self.logger.debug("delete")
        return self.query(sql, where[1] if where and len(where) > 1 else None).rowcount


    def query(self, sql, params = tuple()):
        """Run a raw query
        Args:
            sql: the query string with %s place holder
            params: list or tuple for values.
        Returns:
            cursor: data rows.
        """
#        self.logger.debug(params)
        if not isinstance(params, Iterable):  #added 3/14/18
            params = [params]
#        self.logger.debug("before")  
#        self.logger.debug(params)  
        params = self.list_clean(params)
#        self.logger.debug("after") 
#        self.logger.debug(params)  

        # check if connection is alive. if not, debug
        if self.readonly:
            self.logger.debug(sql%tuple(params))
            self.cur.execute("select 1 from dual where false", None) #fake it.
            self.conn.commit()
            return self.cur
        
        try:
            # print ("%s,%s",(sql, params))
            self.cur.execute(sql, params)
            self.conn.commit() # pymysql does not autocommit
        except pymysql.Error as e:
            # mysql timed out. reconnect and retry once
            if e.args[0] == 2006:
                self.connect()
                self.cur.execute(sql, params)
                self.conn.commit()
            else:
                print("Query failed:\n%s \nError no %s, %s" % (sql%tuple(params), e.args[0],e.args[1]))
                raise
        except:
            self.logger.debug("sql")
            self.logger.debug(sql)
            self.logger.debug("params")
            self.logger.debug(params)            
            raise

        return self.cur

    def commit(self):
        """Commit a transaction (transactional engines like InnoDB require this)"""
        return self.conn.commit()

    def is_open(self):
        """Check if the connection is open"""
        return self.conn.open

    def end(self):
        """Kill the connection"""
        self.cur.close()
        self.conn.close()

    # ===
    
    def replace_nan(self,value):
        """ replace nan values with NULL in dict"""
        return('NULL' if value is None or value != value else value)
    

    def _serialize_insert(self, data):
        """Format insert dict values into strings"""
        keys = "`"+"`,`".join(data.keys())+"`"
        vals = ",".join(["%s" for k in data.keys()])

        return [keys, vals]


    def _serialize_update(self, data):
        """Format update dict values into string"""
        return "=%s,".join( data.keys() ) + "=%s"
    
    def _serialize_where(self, data):
        """Format a where string based on input dictionary"""
        return "=%s and ".join( data.keys() ) + "=%s"


    def _select(self, table=None, fields=(), where=None, order=None, limit=None):
        """Run a select query
        Args:
            table: table name
            fields: list of fields to return
            where: [wherestring, values]
            order: string
            limit: n or [s,n]
        Returns:
            cursor: data rows
        """

        sql = "SELECT %s FROM `%s`" % (",".join(fields), table)

        # where conditions
        if where and len(where) > 0:
            sql += " WHERE %s" % where[0]

        # order
        if order:
            sql += " ORDER BY %s" % order[0]

            if len(order) > 1:
                sql+= " %s" % order[1]

        # limit
        if limit:
            sql += " LIMIT %s" % limit[0]

            if len(limit) > 1:
                sql+= ", %s" % limit[1]
        #        self.logger.debug(sql)
        return self.query(sql, where[1] if where and len(where) > 1 else None)

    def _select_join(self, tables=(), fields=(), join_fields=(), where=None, order=None, limit=None):
        """Run an inner left join query"""

        fields = [tables[0] + "." + f for f in fields[0]] + \
                 [tables[1] + "." + f for f in fields[1]]

        sql = "SELECT %s FROM %s LEFT JOIN %s ON (%s = %s)" % \
                (     ",".join(fields),
                    tables[0],
                    tables[1],
                    tables[0] + "." + join_fields[0], \
                    tables[1] + "." + join_fields[1]
                )

        # where conditions
        if where and len(where) > 0:
            sql += " WHERE %s" % where[0]

        # order
        if order:
            sql += " ORDER BY %s" % order[0]

            if len(order) > 1:
                sql+= " " + order[1]

        # limit
        if limit:
            sql += " LIMIT %s" % limit[0]

            if len(limit) > 1:
                sql+= ", %s" % limit[1]

        return self.query(sql, where[1] if where and len(where) > 1 else None)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.end()

    def list_clean(self,data):
        """cleansing the input data. replacing NaN with None. 
        Args:
            data: list or dictionary (only values matter)
        Returns:
            [values]: List of values or None.
        """
        if(data):
            return([None if value!=value else value for value in data])
            # if value!=value:  # is it NaN type?
        else:
            return(None)